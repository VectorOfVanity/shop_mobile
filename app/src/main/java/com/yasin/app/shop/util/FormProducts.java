package com.yasin.app.shop.util;

import com.yasin.app.shop.model.ProductModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dmytroubogiy on 26.05.17.
 */

public class FormProducts {

    private List<ProductModel> data;
    public List<String> headers;
    public HashMap<String, List<Object>> newItems;

    public FormProducts(List<ProductModel> data) {
        newItems = new HashMap<>();
        this.data = data;
        headers = new ArrayList<>();
        formHashMap();
    }

    private void formHashMap() {
        List<Object> products = new ArrayList<>();
        String currentCategory = "";
        if (data.size() > 0) {
            currentCategory = data.get(0).category.name;
        }
        for (int i = 0; i < data.size(); ++i) {
            if (!currentCategory.equals(data.get(i).category.name)) {
                currentCategory = data.get(i).category.name;
                products = new ArrayList<>();
            }
            newItems.put(data.get(i).category.name, products);
            if (products.contains("+" + data.get(i).subcategory.name)) {
                if (!products.contains(data.get(i))) {
                    products.add(products.indexOf("+" + data.get(i).subcategory.name) + 1, data.get(i));
                }
            } else {
                if (data.get(i).subcategory.name != null && !data.get(i).subcategory.name.isEmpty()) {
                    products.add("+" + data.get(i).subcategory.name);
                }
                if (!products.contains(data.get(i))) {
                    products.add(data.get(i));
                }
                if (data.get(i).subcategory.name == null || data.get(i).subcategory.name.isEmpty()) {
                    if (!products.contains(data.get(i))) {
                        products.add(0, data.get(i));
                    }
                }
            }
            newItems.put(data.get(i).category.name, products);
        }
        for(String key: newItems.keySet()) {
            headers.add(key);
        }
    }
}
