package com.yasin.app.shop.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yasin.app.shop.R;

/**
 * Created by dmytroubogiy on 25.05.17.
 */

public class ProductsViewHolder {

    public TextView productName;
    public TextView productDescription;
    public Button buy;
    public View subcategory;
    public TextView subcategoryTextView;
    public RelativeLayout productContainer;

    public ProductsViewHolder(View view, boolean ifDelete) {
        productName = (TextView) view.findViewById(R.id.textViewProductName);
        productDescription = (TextView) view.findViewById(R.id.textViewProductDescription);
        buy = (Button) view .findViewById(R.id.buttonBuy);
        subcategory = view.findViewById(R.id.subcategory);
        subcategory.setVisibility(View.GONE);
        subcategoryTextView = (TextView) subcategory.findViewById(R.id.textViewChildTitle);
        productContainer = (RelativeLayout) view.findViewById(R.id.productContainer);
        if (ifDelete) {
            buy.setText("Delete");
        }
        else {
            buy.setText("Buy");
        }
    }
}
