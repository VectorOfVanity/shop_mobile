package com.yasin.app.shop.util;

/**
 * Created by dmytroubogiy on 25.05.17.
 */

public enum FragmentType {
    LOGIN_FRAGMENT, SIGN_UP_FRAGMENT, PRODUCT_FRAGMENT, PRODUCT_DETAIL_FRAGMENT;
}
