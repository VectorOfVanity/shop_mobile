package com.yasin.app.shop.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yasin.app.shop.R;

/**
 * Created by dmytroubogiy on 25.05.17.
 */

public class ProductsCategoryViewHolder {

    public TextView category;
    public ImageView point;

    public ProductsCategoryViewHolder(View view) {
        category = (TextView) view.findViewById(R.id.textViewCategory);
        point = (ImageView) view.findViewById(R.id.imageViewPoint);
    }
}
