package com.yasin.app.shop.manger.response;

/**
 * Created by dmytroubogiy on 26.05.17.
 */

public class ResponseStatus {

    public boolean hasError;
    public int code;
    public String message;
}
