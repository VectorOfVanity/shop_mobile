package com.yasin.app.shop.manger.response;

import com.yasin.app.shop.model.ProductModel;

import java.util.List;

/**
 * Created by dmytroubogiy on 26.05.17.
 */

public class ProductsResponse extends ResponseStatus {
    public List<ProductModel> products;
}
