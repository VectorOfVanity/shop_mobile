package com.yasin.app.shop.manger;

import com.yasin.app.shop.manger.request.BuyRequest;
import com.yasin.app.shop.manger.response.LoginResponse;
import com.yasin.app.shop.manger.response.ProductsResponse;
import com.yasin.app.shop.manger.response.ResponseStatus;
import com.yasin.app.shop.model.ProductModel;
import com.yasin.app.shop.model.User;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by dmytroubogiy on 25.05.17.
 */

public interface RestService {

    @POST("user/create")
    Call<LoginResponse> signUp(@Body User user);

    @POST("user/submitLogin")
    Call<LoginResponse> login(@Body User user);

    @GET("Product/list")
    Call<ProductsResponse> getProducts();

    @POST("product/buy")
    Call<ResponseStatus> buyProduct(@Body BuyRequest id);

    @POST("bought/list")
    Call<ProductsResponse> getOrders(@Body BuyRequest orders);

    @POST("bought/delete")
    Call<ResponseStatus> deleteOrder(@Body BuyRequest deleteOrder);



}
