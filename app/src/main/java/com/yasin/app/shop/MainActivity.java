package com.yasin.app.shop;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.yasin.app.shop.fragment.ProductsFragment;
import com.yasin.app.shop.manger.ApiClient;
import com.yasin.app.shop.util.FragmentType;

public class MainActivity extends AppCompatActivity {

    private ViewGroup segment;
    public FragmentType currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        segment = (ViewGroup) findViewById(R.id.header);
        ApiClient.getInstance().getProducts();
        transactionFragments(ProductsFragment.getInstance(), R.id.mainContainer);
    }

    public void hideSegment() {
        segment.setVisibility(View.GONE);
    }

    public void showSegment() {
        segment.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        switch (currentFragment) {
            case PRODUCT_DETAIL_FRAGMENT:
                showSegment();
                currentFragment = FragmentType.PRODUCT_FRAGMENT;
                transactionFragments(ProductsFragment.getInstance(), R.id.mainContainer);
                break;
            default:
                super.onBackPressed();
        }
    }

    public void transactionFragments(Fragment fragment, int viewResource) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (!fragment.isAdded()) {
            ft.replace(viewResource, fragment);
            ft.commitAllowingStateLoss();
        }
    }


    public ViewGroup getSegment() {
        return segment;
    }
}
