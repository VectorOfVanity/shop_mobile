package com.yasin.app.shop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import com.yasin.app.shop.MainActivity;
import com.yasin.app.shop.R;
import com.yasin.app.shop.fragment.ProductDetailFragment;
import com.yasin.app.shop.fragment.ProductsFragment;
import com.yasin.app.shop.manger.ApiClient;
import com.yasin.app.shop.model.ProductModel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Created by dmytroubogiy on 25.05.17.
 */

public class ProductsAdapter extends BaseExpandableListAdapter {

    private LayoutInflater inflater;
    private List<String> categories;
    private HashMap<String, List<Object>> dataChild;
    private ProductsCategoryViewHolder productsCategoryViewHolder;
    private ProductsViewHolder productsViewHolder;
    private Set<String> sentItems;
    private boolean ifDelete;

    public ProductsAdapter(Context context, List<String> listDataHeaders, HashMap<String, List<Object>> listDataChild, boolean ifDelete) {
        inflater = LayoutInflater.from(context);
        this.categories = listDataHeaders;
        this.dataChild = listDataChild;
        sentItems = new HashSet<>();
        this.ifDelete = ifDelete;
    }

    @Override
    public int getGroupCount() {
        return this.categories.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.dataChild.get(this.categories.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.categories.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.dataChild.get(this.categories.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String item = (String) getGroup(groupPosition);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.category_item, null);
            productsCategoryViewHolder = new ProductsCategoryViewHolder(convertView);
            convertView.setTag(productsCategoryViewHolder);
        }
        else {
            productsCategoryViewHolder = (ProductsCategoryViewHolder) convertView.getTag();
        }
        productsCategoryViewHolder.category.setText(item);
        if (!isExpanded) {
            productsCategoryViewHolder.point.setImageResource(R.drawable.close);
        }
        else {
            productsCategoryViewHolder.point.setImageResource(R.drawable.open);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Object item = getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.product_item, null);
            productsViewHolder = new ProductsViewHolder(convertView, ifDelete);
            convertView.setTag(productsViewHolder);
        } else {
            productsViewHolder = (ProductsViewHolder) convertView.getTag();
        }
        if (item instanceof String) {
            String title = ((String) item).replace("+", "");
            productsViewHolder.subcategory.setVisibility(View.VISIBLE);
            productsViewHolder.productContainer.setVisibility(View.GONE);
            productsViewHolder.subcategoryTextView.setText(title);
        }
        else {
            productsViewHolder.productContainer.setVisibility(View.VISIBLE);
            productsViewHolder.subcategory.setVisibility(View.GONE);
            ProductModel productModel = (ProductModel) item;
            productsViewHolder.productContainer.setOnClickListener(openDetail(productModel));
            productsViewHolder.productName.setText(productModel.name);
            productsViewHolder.productDescription.setText(productModel.price);
            if (!ifDelete) {
                productsViewHolder.buy.setOnClickListener(buy(productModel));
            }
            else {
                productsViewHolder.buy.setOnClickListener(delete(productModel));
            }
        }
        return convertView;
    }

    public View.OnClickListener buy(final ProductModel model) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ProductsFragment.getInstance().showDialog("Buy...");
                ApiClient.getInstance().buyProduct(model.id);
            }
        };
    }

    public View.OnClickListener delete(final ProductModel model) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductsFragment.getInstance().showDialog("Delete...");
                ApiClient.getInstance().deleteOrder(model.order);
            }
        };
    }

    public View.OnClickListener openDetail(final ProductModel model) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductDetailFragment.model = (model);
                ((MainActivity) (v.getContext())).transactionFragments(ProductDetailFragment.getInstance(), R.id.mainContainer);
            }
        };
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void clear() {
        sentItems.clear();
        categories.clear();
        dataChild.clear();
        notifyDataSetChanged();
    }
}
