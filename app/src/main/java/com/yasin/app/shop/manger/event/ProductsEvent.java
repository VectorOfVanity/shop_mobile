package com.yasin.app.shop.manger.event;

import com.yasin.app.shop.manger.response.ProductsResponse;
import com.yasin.app.shop.model.ProductModel;

import java.util.List;

/**
 * Created by dmytroubogiy on 26.05.17.
 */

public class ProductsEvent {
    public ProductsResponse products;
}
