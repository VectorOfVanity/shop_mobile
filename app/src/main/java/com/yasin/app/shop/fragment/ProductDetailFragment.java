package com.yasin.app.shop.fragment;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.yasin.app.shop.MainActivity;
import com.yasin.app.shop.R;
import com.yasin.app.shop.ShopApp;
import com.yasin.app.shop.manger.ApiClient;
import com.yasin.app.shop.model.ProductModel;
import com.yasin.app.shop.util.FragmentType;

/**
 * Created by dmytroubogiy on 28.05.17.
 */

public class ProductDetailFragment extends Fragment {

    private static ProductDetailFragment instance;
    private ImageView productImage;
    private ImageLoader imageLoader;
    public static ProductModel model;

    public ProductDetailFragment() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(ShopApp.getContext()));
    }

    public static ProductDetailFragment getInstance() {
        if (instance == null) {
            instance = new ProductDetailFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity) getActivity()).currentFragment = FragmentType.PRODUCT_DETAIL_FRAGMENT;
        ((MainActivity) getActivity()).hideSegment();
        productImage = (ImageView) view.findViewById(R.id.imageViewProduct);
        if (model.url != null) {
            loadImage(ApiClient.getInstance().baseUrlImage + model.url, productImage);
        }
        TextView name, price;
        name = (TextView) view.findViewById(R.id.textViewName);
        name.setText(model.name);
        price = (TextView) view.findViewById(R.id.textViewPrice);
        price.setText(model.price);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_detail, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void loadImage(String url, final ImageView imageView) {
        imageLoader.loadImage(url, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                Log.d("image_loading", "success");
                imageView.setImageBitmap(loadedImage);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                super.onLoadingFailed(imageUri, view, failReason);
                Log.d("image_loading", "failed");
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                super.onLoadingCancelled(imageUri, view);
                Log.d("image_loading", "canceled");
            }
        });
    }
}
