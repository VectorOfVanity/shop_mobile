package com.yasin.app.shop;

import android.app.Application;
import android.content.Context;

/**
 * Created by dmytroubogiy on 28.05.17.
 */

public class ShopApp extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public static Context getContext() {
        return context;
    }
}
