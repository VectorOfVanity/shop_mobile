package com.yasin.app.shop.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.yasin.app.shop.MainActivity;
import com.yasin.app.shop.R;
import com.yasin.app.shop.manger.ApiClient;
import com.yasin.app.shop.manger.event.BaseErrorEvent;
import com.yasin.app.shop.manger.event.BaseEvent;
import com.yasin.app.shop.manger.event.UserEvent;
import com.yasin.app.shop.model.User;

import de.greenrobot.event.EventBus;

/**
 * Created by dmytroubogiy on 25.05.17.
 */

public class LoginFragment extends Fragment{

    private static LoginFragment instance;
    private EditText emailEditText, passwordEditText;
    private Button loginBtn;
    private ProgressDialog load;

    public LoginFragment() {

    }

    public static LoginFragment getInstance() {
        if (instance == null) {
            instance = new LoginFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    showDialog("Login...");
                    User user = new User();
                    user.setEmail(emailEditText.getText().toString());
                    user.setPassword(passwordEditText.getText().toString());
                    ApiClient.getInstance().login(user);
                }
            }
        });
    }

    public void onEvent(UserEvent event) {
        dismiss();
        ApiClient.getInstance().setUser(event.loginResponse.user);
        Intent goToProducts = new Intent(getActivity(), MainActivity.class);
        startActivity(goToProducts);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onAttach(Context context) {
        EventBus.getDefault().register(this);
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    private void initViews(View view) {
        emailEditText = (EditText) view.findViewById(R.id.editTextEmail);
        passwordEditText = (EditText) view.findViewById(R.id.editTextPassword);
        loginBtn = (Button) view.findViewById(R.id.buttonLogin);
    }

    private boolean validate() {
        if (validateEmail()) {
            if (validatePassword()) {
                return true;
            }
        }
        return false;
    }

    public void showDialog(String message) {
        load = ProgressDialog.show(getActivity(), message, null);
    }

    private void dismiss() {
        load.dismiss();
    }

    private boolean validatePassword() {
        String password = passwordEditText.getText().toString();
        if (!TextUtils.isEmpty(password) && password.length() > 4) {
            passwordEditText.setHintTextColor(Color.BLUE);
            return true;
        }
        passwordEditText.setHintTextColor(Color.RED);
        showError("Password too short");
        return false;
    }

    private boolean validateEmail() {
        String email = emailEditText.getText().toString();
        if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailEditText.setHintTextColor(Color.BLUE);
            return true;
        }
        emailEditText.setHintTextColor(Color.RED);
        showError("Wrong email");
        return false;
    }

    public void onEvent(BaseErrorEvent errorEvent) {
        dismiss();
        EventBus.getDefault().removeStickyEvent(errorEvent);
        showError(errorEvent.message);
    }

    private void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
