package com.yasin.app.shop.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.yasin.app.shop.MainActivity;
import com.yasin.app.shop.R;
import com.yasin.app.shop.manger.ApiClient;
import com.yasin.app.shop.manger.event.BaseErrorEvent;
import com.yasin.app.shop.manger.event.BaseEvent;
import com.yasin.app.shop.manger.event.UserEvent;
import com.yasin.app.shop.model.User;

import de.greenrobot.event.EventBus;

/**
 * Created by dmytroubogiy on 25.05.17.
 */

public class SignUpFragment extends Fragment {

    private static SignUpFragment instance;
    private EditText firstNameEditText, secondNameEditText, emailEditText, passwordEditText;
    private Button signUpBtn;

    //requires public constructor
    public SignUpFragment() {

    }

    public static SignUpFragment getInstance() {
        if (instance == null) {
            instance = new SignUpFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        EventBus.getDefault().register(this);
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(UserEvent event) {
        ApiClient.getInstance().setUser(event.loginResponse.user);
        Intent goToProducts = new Intent(getActivity(), MainActivity.class);
        startActivity(goToProducts);
    }

    private void initViews(View view) {
        firstNameEditText = (EditText) view.findViewById(R.id.editTextFirstName);
        secondNameEditText = (EditText) view.findViewById(R.id.editTextSecondName);
        emailEditText = (EditText) view.findViewById(R.id.editTextEmail);
        passwordEditText = (EditText) view.findViewById(R.id.editTextPassword);
        signUpBtn = (Button) view.findViewById(R.id.buttonSignUp);
    }

    private void signUp() {
        if (validate()) {
            User user = new User();
            user.setName(firstNameEditText.getText().toString());
            user.setSurname(secondNameEditText.getText().toString());
            user.setPassword(passwordEditText.getText().toString());
            user.setEmail(emailEditText.getText().toString());
            ApiClient.getInstance().signUp(user);
        }
    }

    private boolean validate() {
        if (validateFirstName()) {
            if (validateLastName()) {
                if (validatePassword()) {
                    if (validateEmail()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean validateFirstName() {
        String firstName = firstNameEditText.getText().toString();
        if (!TextUtils.isEmpty(firstName)) {
            firstNameEditText.setHintTextColor(Color.BLUE);
            return true;
        }
        firstNameEditText.setHintTextColor(Color.RED);
        showError("Enter first name");
        return false;
    }

    private boolean validateLastName() {
        String secondName = secondNameEditText.getText().toString();
        if (!TextUtils.isEmpty(secondName)) {
            secondNameEditText.setHintTextColor(Color.BLUE);
            return true;
        }
        secondNameEditText.setHintTextColor(Color.RED);
        showError("Enter second name");
        return false;
    }

    private boolean validatePassword() {
        String password = passwordEditText.getText().toString();
        if (!TextUtils.isEmpty(password) && password.length() > 4) {
            passwordEditText.setHintTextColor(Color.BLUE);
            return true;
        }
        passwordEditText.setHintTextColor(Color.RED);
        showError("Password too short");
        return false;
    }

    private boolean validateEmail() {
        String email = emailEditText.getText().toString();
        if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailEditText.setHintTextColor(Color.BLUE);
            return true;
        }
        emailEditText.setHintTextColor(Color.RED);
        showError("Wrong email");
        return false;
    }

    public void onEvent(BaseErrorEvent errorEvent) {
        EventBus.getDefault().removeStickyEvent(errorEvent);
        showError(errorEvent.message);
    }

    private void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
