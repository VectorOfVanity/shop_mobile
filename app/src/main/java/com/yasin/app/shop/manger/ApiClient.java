package com.yasin.app.shop.manger;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.yasin.app.shop.manger.event.BaseErrorEvent;
import com.yasin.app.shop.manger.event.BaseEvent;
import com.yasin.app.shop.manger.event.DeleteOrderEvent;
import com.yasin.app.shop.manger.event.OrdersEvent;
import com.yasin.app.shop.manger.event.ProductsEvent;
import com.yasin.app.shop.manger.event.UserEvent;
import com.yasin.app.shop.manger.request.BuyRequest;
import com.yasin.app.shop.manger.response.LoginResponse;
import com.yasin.app.shop.manger.response.ProductsResponse;
import com.yasin.app.shop.manger.response.ResponseStatus;
import com.yasin.app.shop.model.User;
import com.yasin.app.shop.util.ApiName;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by dmytroubogiy on 25.05.17.
 */

public class ApiClient {
    private static ApiClient instance;
    public String baseUrl;
    private Retrofit retrofit;
    private RestService service;
    public String baseUrlImage;
    private User user;

    private ApiClient() {
        this.baseUrl = "http://54.202.127.2/";
        this.baseUrlImage = "http://54.202.127.2";
        OkHttpClient.Builder httpClient = createClient();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
//                        .header("Accept", "application/json")
//                        .header("Content-Type", "application/json")
                        //.header("Authorization", "Bearer " + token)
                        .method(original.method(), original.body());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        httpClient.addInterceptor(logging);
        Gson gson = new GsonBuilder()
                .create();
        retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        service = retrofit.create(RestService.class);
    }

    public static ApiClient getInstance(){
        if(instance == null){
            instance = new ApiClient();
        }
        return instance;
    }

    private OkHttpClient.Builder createClient() {
        final OkHttpClient.Builder client = new OkHttpClient().newBuilder();
        client.readTimeout(60, TimeUnit.SECONDS);
        client.connectTimeout(60, TimeUnit.SECONDS);
        return client;
    }

    public void signUp(User user) {
        Call call = service.signUp(user);
        RequestCalls request = new RequestCalls();
        request.setApiName(ApiName.SIGN_UP);
        request.execute(call);
    }

    public void login(User user) {
        Call call = service.login(user);
        RequestCalls request = new RequestCalls();
        request.setApiName(ApiName.LOGIN);
        request.execute(call);
    }

    public void getProducts() {
        Call call = service.getProducts();
        RequestCalls request = new RequestCalls();
        request.setApiName(ApiName.PRODUCTS_LIST);
        request.execute(call);
    }

    public void buyProduct(String id) {
        BuyRequest buyRequest = new BuyRequest();
        buyRequest.id = id;
        buyRequest.userid = ApiClient.getInstance().getUser().getId();
        Call call = service.buyProduct(buyRequest);
        RequestCalls requestCalls = new RequestCalls();
        requestCalls.setApiName(ApiName.BUY);
        requestCalls.execute(call);
    }

    public void getOrders() {
        BuyRequest orders = new BuyRequest();
        orders.id = ApiClient.getInstance().getUser().getId();
        Call call = service.getOrders(orders);
        RequestCalls requestCalls = new RequestCalls();
        requestCalls.setApiName(ApiName.ORDERS);
        requestCalls.execute(call);
    }

    public void deleteOrder(String orderId) {
        BuyRequest buyRequest = new BuyRequest();
        buyRequest.userid = ApiClient.getInstance().getUser().getId();
        buyRequest.id = orderId;
        Call call = service.deleteOrder(buyRequest);
        RequestCalls requestCalls = new RequestCalls();
        requestCalls.setApiName(ApiName.DELETE_ORDER);
        requestCalls.execute(call);
    }

    private class RequestCalls extends AsyncTask<Call, Void, Void> {

        private ApiName apiName;

        @Override
        protected Void doInBackground(Call... baseRequests) {

            baseRequests[0].enqueue(new Callback() {
                @Override
                public void onResponse(Response response) {
                    checkResponse(response, apiName);
                    Log.d("COOOOOOOOOODE", response.message());
                }

                @Override
                public void onFailure(Throwable t) {
                    error("Connection problems");
                }
            });

            return null;
        }

        public void setApiName(ApiName apiName) {
            this.apiName = apiName;
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    private void error(String message) {
        BaseErrorEvent errorEvent = new BaseErrorEvent();
        errorEvent.message = message;
        EventBus.getDefault().postSticky(errorEvent);
    }

    private void checkResponse(Response response, ApiName apiName) {
        switch (apiName) {
            case LOGIN:
            case SIGN_UP:
                LoginResponse status = (LoginResponse) response.body();
                if (!status.hasError) {
                    UserEvent userEvent = new UserEvent();
                    userEvent.loginResponse = status;
                    EventBus.getDefault().postSticky(userEvent);
                }
                else {
                    error("Wrong data");
                }
                break;
            case PRODUCTS_LIST:
                ProductsResponse productsResponse = (ProductsResponse) response.body();
                if (!productsResponse.hasError) {
                    ProductsEvent event = new ProductsEvent();
                    event.products = productsResponse;
                    EventBus.getDefault().postSticky(event);
                }
                else {
                    error("Connection problems");
                }
                break;
            case BUY:
                ResponseStatus statusBuy = (ResponseStatus) response.body();
                if (!statusBuy.hasError) {
                    BaseEvent event = new BaseEvent();
                    EventBus.getDefault().postSticky(event);
                }
                else {
                    error("Connection problems");
                }
                break;
            case ORDERS:
                ProductsResponse ordersResponse = (ProductsResponse) response.body();
                if (!ordersResponse.hasError) {
                    OrdersEvent event = new OrdersEvent();
                    event.productsResponse = ordersResponse;
                    EventBus.getDefault().postSticky(event);
                }
                else {
                    error("Connection problems");
                }
                break;
            case DELETE_ORDER:
                ResponseStatus statusDelete = (ResponseStatus) response.body();
                if (!statusDelete.hasError) {
                    DeleteOrderEvent deleteOrderEvent = new DeleteOrderEvent();
                    deleteOrderEvent.status = statusDelete;
                    EventBus.getDefault().postSticky(deleteOrderEvent);
                }
                break;
            default: break;
        }


    }
}
