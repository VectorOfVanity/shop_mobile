package com.yasin.app.shop.adapter;

import android.view.View;
import android.widget.TextView;

import com.yasin.app.shop.R;

/**
 * Created by dmytroubogiy on 25.05.17.
 */

public class ProductSubcategoryViewHolder {

    public TextView subcategory;

    public ProductSubcategoryViewHolder(View view) {
        subcategory = (TextView) view.findViewById(R.id.textViewChildTitle);
    }
}
