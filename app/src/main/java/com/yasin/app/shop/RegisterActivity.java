package com.yasin.app.shop;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.yasin.app.shop.fragment.LoginFragment;
import com.yasin.app.shop.fragment.SignUpFragment;
import com.yasin.app.shop.util.FragmentType;

public class RegisterActivity extends AppCompatActivity {

    private TextView signUpBtn;
    private FragmentType currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        signUpBtn = (TextView) findViewById(R.id.textViewSignUp);
        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentFragment == FragmentType.LOGIN_FRAGMENT) {
                    openSignUp();
                }
                else if (currentFragment == FragmentType.SIGN_UP_FRAGMENT) {
                    openLogin();
                }
            }
        });
        openLogin();
    }

    public void transactionFragments(Fragment fragment, int viewResource) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (!fragment.isAdded()) {
            ft.replace(viewResource, fragment);
            ft.commitAllowingStateLoss();
        }
    }

    public void openSignUp() {
        currentFragment = FragmentType.SIGN_UP_FRAGMENT;
        signUpBtn.setText("Login");
        transactionFragments(SignUpFragment.getInstance(), R.id.registerContainer);
    }

    public void openLogin() {
        currentFragment = FragmentType.LOGIN_FRAGMENT;
        signUpBtn.setText("Sign up");
        transactionFragments(LoginFragment.getInstance(), R.id.registerContainer);
    }
}
