package com.yasin.app.shop.util;

/**
 * Created by dmytroubogiy on 26.05.17.
 */

public enum ApiName {
    SIGN_UP, LOGIN, PRODUCTS_LIST, ORDERS, BUY, DELETE_ORDER;
}
