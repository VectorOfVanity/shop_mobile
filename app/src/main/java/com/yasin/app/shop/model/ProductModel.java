package com.yasin.app.shop.model;

/**
 * Created by dmytroubogiy on 25.05.17.
 */

public class ProductModel {

    public String id;
    public String name;
    public Category category;
    public Subcategory subcategory;
    public String price;
    public String url;
    public String order;
}
