package com.yasin.app.shop.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.yasin.app.shop.MainActivity;
import com.yasin.app.shop.R;
import com.yasin.app.shop.adapter.ProductsAdapter;
import com.yasin.app.shop.manger.ApiClient;
import com.yasin.app.shop.manger.event.BaseErrorEvent;
import com.yasin.app.shop.manger.event.BaseEvent;
import com.yasin.app.shop.manger.event.DeleteOrderEvent;
import com.yasin.app.shop.manger.event.OrdersEvent;
import com.yasin.app.shop.manger.event.ProductsEvent;
import com.yasin.app.shop.model.ProductModel;
import com.yasin.app.shop.util.FormProducts;
import com.yasin.app.shop.util.FragmentType;

import java.util.HashMap;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by dmytroubogiy on 25.05.17.
 */

public class ProductsFragment extends Fragment{

    private static ProductsFragment instance;
    private ExpandableListView productsList;
    private List<String> headers;
    private HashMap<String, List<Object>> items;
    private ProductsAdapter adapter;
    private boolean ifProducts;
    private ProgressDialog load;

    public ProductsFragment() {

    }

    public static ProductsFragment getInstance() {
        if (instance == null) {
            instance = new ProductsFragment();
        }
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        return inflater.inflate(R.layout.fragment_products, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RadioButton products = (RadioButton) ((MainActivity) getActivity()).getSegment().findViewById(R.id.buttonActuale);
        ((MainActivity) getActivity()).currentFragment = FragmentType.PRODUCT_FRAGMENT;
        products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ifProducts) {
                    showDialog("Loading...");
                    ApiClient.getInstance().getProducts();
                }
            }
        });
        RadioButton orders = (RadioButton) ((MainActivity) getActivity()).getSegment().findViewById(R.id.buttonPast);
        orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ifProducts) {
                    showDialog("Loading...");
                    ApiClient.getInstance().getOrders();
                }
            }
        });
        productsList = (ExpandableListView) view.findViewById(R.id.listViewProducts);
    }

    @Override
    public void onResume() {
        super.onResume();
        showDialog("Loading...");
        ApiClient.getInstance().getProducts();
    }

    public void showDialog(String message) {
        load = ProgressDialog.show(getActivity(), message, null);
    }

    private void dismiss() {
        if (load != null) {
            load.dismiss();
        }
    }

    public void onEvent(ProductsEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        ifProducts = true;
        dismiss();
        FormProducts formProducts = new FormProducts(event.products.products);
        headers = formProducts.headers;
        items = formProducts.newItems;
        adapter = new ProductsAdapter(getActivity(), headers, items, false);
        productsList.setAdapter(adapter);
    }

    public void onEvent(OrdersEvent event) {
        dismiss();
        ifProducts = false;
        EventBus.getDefault().removeStickyEvent(event);
        adapter.clear();
        FormProducts formProducts = new FormProducts(event.productsResponse.products);
        headers = formProducts.headers;
        items = formProducts.newItems;
        adapter = new ProductsAdapter(getActivity(), headers, items, true);
        productsList.setAdapter(adapter);
    }

    public void onEvent(BaseEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        dismiss();
    }

    public void onEvent(BaseErrorEvent errorEvent) {
        EventBus.getDefault().removeStickyEvent(errorEvent);
        showError(errorEvent.message);
    }

    public void onEvent(DeleteOrderEvent deleteOrderEvent) {
        EventBus.getDefault().removeStickyEvent(deleteOrderEvent);
        dismiss();
        ApiClient.getInstance().getOrders();
    }

    private void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
}
